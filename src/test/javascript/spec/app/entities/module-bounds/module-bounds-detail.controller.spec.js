'use strict';

describe('Controller Tests', function() {

    describe('ModuleBounds Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockModuleBounds, MockModule, MockUserDashboard;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockModuleBounds = jasmine.createSpy('MockModuleBounds');
            MockModule = jasmine.createSpy('MockModule');
            MockUserDashboard = jasmine.createSpy('MockUserDashboard');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ModuleBounds': MockModuleBounds,
                'Module': MockModule,
                'UserDashboard': MockUserDashboard
            };
            createController = function() {
                $injector.get('$controller')("ModuleBoundsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'openBoxApp:moduleBoundsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
