'use strict';

describe('Controller Tests', function() {

    describe('UserDashboard Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockUserDashboard, MockUser, MockModuleBounds;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockUserDashboard = jasmine.createSpy('MockUserDashboard');
            MockUser = jasmine.createSpy('MockUser');
            MockModuleBounds = jasmine.createSpy('MockModuleBounds');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'UserDashboard': MockUserDashboard,
                'User': MockUser,
                'ModuleBounds': MockModuleBounds
            };
            createController = function() {
                $injector.get('$controller')("UserDashboardDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'openBoxApp:userDashboardUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
