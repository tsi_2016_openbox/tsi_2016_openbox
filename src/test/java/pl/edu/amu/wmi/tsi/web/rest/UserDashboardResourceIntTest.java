package pl.edu.amu.wmi.tsi.web.rest;

import pl.edu.amu.wmi.tsi.OpenBoxApp;
import pl.edu.amu.wmi.tsi.domain.UserDashboard;
import pl.edu.amu.wmi.tsi.repository.UserDashboardRepository;
import pl.edu.amu.wmi.tsi.web.rest.dto.UserDashboardDTO;
import pl.edu.amu.wmi.tsi.web.rest.mapper.UserDashboardMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the UserDashboardResource REST controller.
 *
 * @see UserDashboardResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OpenBoxApp.class)
@WebAppConfiguration
@IntegrationTest
public class UserDashboardResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final Integer DEFAULT_COLUMNS = 1;
    private static final Integer UPDATED_COLUMNS = 2;

    @Inject
    private UserDashboardRepository userDashboardRepository;

    @Inject
    private UserDashboardMapper userDashboardMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUserDashboardMockMvc;

    private UserDashboard userDashboard;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserDashboardResource userDashboardResource = new UserDashboardResource();
        ReflectionTestUtils.setField(userDashboardResource, "userDashboardRepository", userDashboardRepository);
        ReflectionTestUtils.setField(userDashboardResource, "userDashboardMapper", userDashboardMapper);
        this.restUserDashboardMockMvc = MockMvcBuilders.standaloneSetup(userDashboardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        userDashboard = new UserDashboard();
        userDashboard.setName(DEFAULT_NAME);
        userDashboard.setColumns(DEFAULT_COLUMNS);
    }

    @Test
    @Transactional
    public void createUserDashboard() throws Exception {
        int databaseSizeBeforeCreate = userDashboardRepository.findAll().size();

        // Create the UserDashboard
        UserDashboardDTO userDashboardDTO = userDashboardMapper.userDashboardToUserDashboardDTO(userDashboard);

        restUserDashboardMockMvc.perform(post("/api/user-dashboards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDashboardDTO)))
                .andExpect(status().isCreated());

        // Validate the UserDashboard in the database
        List<UserDashboard> userDashboards = userDashboardRepository.findAll();
        assertThat(userDashboards).hasSize(databaseSizeBeforeCreate + 1);
        UserDashboard testUserDashboard = userDashboards.get(userDashboards.size() - 1);
        assertThat(testUserDashboard.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserDashboard.getColumns()).isEqualTo(DEFAULT_COLUMNS);
    }

    @Test
    @Transactional
    public void getAllUserDashboards() throws Exception {
        // Initialize the database
        userDashboardRepository.saveAndFlush(userDashboard);

        // Get all the userDashboards
        restUserDashboardMockMvc.perform(get("/api/user-dashboards?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userDashboard.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].columns").value(hasItem(DEFAULT_COLUMNS)));
    }

    @Test
    @Transactional
    public void getUserDashboard() throws Exception {
        // Initialize the database
        userDashboardRepository.saveAndFlush(userDashboard);

        // Get the userDashboard
        restUserDashboardMockMvc.perform(get("/api/user-dashboards/{id}", userDashboard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(userDashboard.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.columns").value(DEFAULT_COLUMNS));
    }

    @Test
    @Transactional
    public void getNonExistingUserDashboard() throws Exception {
        // Get the userDashboard
        restUserDashboardMockMvc.perform(get("/api/user-dashboards/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserDashboard() throws Exception {
        // Initialize the database
        userDashboardRepository.saveAndFlush(userDashboard);
        int databaseSizeBeforeUpdate = userDashboardRepository.findAll().size();

        // Update the userDashboard
        UserDashboard updatedUserDashboard = new UserDashboard();
        updatedUserDashboard.setId(userDashboard.getId());
        updatedUserDashboard.setName(UPDATED_NAME);
        updatedUserDashboard.setColumns(UPDATED_COLUMNS);
        UserDashboardDTO userDashboardDTO = userDashboardMapper.userDashboardToUserDashboardDTO(updatedUserDashboard);

        restUserDashboardMockMvc.perform(put("/api/user-dashboards")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDashboardDTO)))
                .andExpect(status().isOk());

        // Validate the UserDashboard in the database
        List<UserDashboard> userDashboards = userDashboardRepository.findAll();
        assertThat(userDashboards).hasSize(databaseSizeBeforeUpdate);
        UserDashboard testUserDashboard = userDashboards.get(userDashboards.size() - 1);
        assertThat(testUserDashboard.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserDashboard.getColumns()).isEqualTo(UPDATED_COLUMNS);
    }

    @Test
    @Transactional
    public void deleteUserDashboard() throws Exception {
        // Initialize the database
        userDashboardRepository.saveAndFlush(userDashboard);
        int databaseSizeBeforeDelete = userDashboardRepository.findAll().size();

        // Get the userDashboard
        restUserDashboardMockMvc.perform(delete("/api/user-dashboards/{id}", userDashboard.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<UserDashboard> userDashboards = userDashboardRepository.findAll();
        assertThat(userDashboards).hasSize(databaseSizeBeforeDelete - 1);
    }
}
