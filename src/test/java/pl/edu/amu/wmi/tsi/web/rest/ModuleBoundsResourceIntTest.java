package pl.edu.amu.wmi.tsi.web.rest;

import pl.edu.amu.wmi.tsi.OpenBoxApp;
import pl.edu.amu.wmi.tsi.domain.ModuleBounds;
import pl.edu.amu.wmi.tsi.repository.ModuleBoundsRepository;
import pl.edu.amu.wmi.tsi.web.rest.dto.ModuleBoundsDTO;
import pl.edu.amu.wmi.tsi.web.rest.mapper.ModuleBoundsMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ModuleBoundsResource REST controller.
 *
 * @see ModuleBoundsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OpenBoxApp.class)
@WebAppConfiguration
@IntegrationTest
public class ModuleBoundsResourceIntTest {


    private static final Integer DEFAULT_ROW_POS = 1;
    private static final Integer UPDATED_ROW_POS = 2;

    private static final Integer DEFAULT_COL_POS = 1;
    private static final Integer UPDATED_COL_POS = 2;

    private static final Integer DEFAULT_ROW_SPAN = 1;
    private static final Integer UPDATED_ROW_SPAN = 2;

    private static final Integer DEFAULT_COL_SPAN = 1;
    private static final Integer UPDATED_COL_SPAN = 2;

    @Inject
    private ModuleBoundsRepository moduleBoundsRepository;

    @Inject
    private ModuleBoundsMapper moduleBoundsMapper;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restModuleBoundsMockMvc;

    private ModuleBounds moduleBounds;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ModuleBoundsResource moduleBoundsResource = new ModuleBoundsResource();
        ReflectionTestUtils.setField(moduleBoundsResource, "moduleBoundsRepository", moduleBoundsRepository);
        ReflectionTestUtils.setField(moduleBoundsResource, "moduleBoundsMapper", moduleBoundsMapper);
        this.restModuleBoundsMockMvc = MockMvcBuilders.standaloneSetup(moduleBoundsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        moduleBounds = new ModuleBounds();
        moduleBounds.setRowPos(DEFAULT_ROW_POS);
        moduleBounds.setColPos(DEFAULT_COL_POS);
        moduleBounds.setRowSpan(DEFAULT_ROW_SPAN);
        moduleBounds.setColSpan(DEFAULT_COL_SPAN);
    }

    @Test
    @Transactional
    public void createModuleBounds() throws Exception {
        int databaseSizeBeforeCreate = moduleBoundsRepository.findAll().size();

        // Create the ModuleBounds
        ModuleBoundsDTO moduleBoundsDTO = moduleBoundsMapper.moduleBoundsToModuleBoundsDTO(moduleBounds);

        restModuleBoundsMockMvc.perform(post("/api/module-bounds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moduleBoundsDTO)))
                .andExpect(status().isCreated());

        // Validate the ModuleBounds in the database
        List<ModuleBounds> moduleBounds = moduleBoundsRepository.findAll();
        assertThat(moduleBounds).hasSize(databaseSizeBeforeCreate + 1);
        ModuleBounds testModuleBounds = moduleBounds.get(moduleBounds.size() - 1);
        assertThat(testModuleBounds.getRowPos()).isEqualTo(DEFAULT_ROW_POS);
        assertThat(testModuleBounds.getColPos()).isEqualTo(DEFAULT_COL_POS);
        assertThat(testModuleBounds.getRowSpan()).isEqualTo(DEFAULT_ROW_SPAN);
        assertThat(testModuleBounds.getColSpan()).isEqualTo(DEFAULT_COL_SPAN);
    }

    @Test
    @Transactional
    public void getAllModuleBounds() throws Exception {
        // Initialize the database
        moduleBoundsRepository.saveAndFlush(moduleBounds);

        // Get all the moduleBounds
        restModuleBoundsMockMvc.perform(get("/api/module-bounds?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(moduleBounds.getId().intValue())))
                .andExpect(jsonPath("$.[*].rowPos").value(hasItem(DEFAULT_ROW_POS)))
                .andExpect(jsonPath("$.[*].colPos").value(hasItem(DEFAULT_COL_POS)))
                .andExpect(jsonPath("$.[*].rowSpan").value(hasItem(DEFAULT_ROW_SPAN)))
                .andExpect(jsonPath("$.[*].colSpan").value(hasItem(DEFAULT_COL_SPAN)));
    }

    @Test
    @Transactional
    public void getModuleBounds() throws Exception {
        // Initialize the database
        moduleBoundsRepository.saveAndFlush(moduleBounds);

        // Get the moduleBounds
        restModuleBoundsMockMvc.perform(get("/api/module-bounds/{id}", moduleBounds.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(moduleBounds.getId().intValue()))
            .andExpect(jsonPath("$.rowPos").value(DEFAULT_ROW_POS))
            .andExpect(jsonPath("$.colPos").value(DEFAULT_COL_POS))
            .andExpect(jsonPath("$.rowSpan").value(DEFAULT_ROW_SPAN))
            .andExpect(jsonPath("$.colSpan").value(DEFAULT_COL_SPAN));
    }

    @Test
    @Transactional
    public void getNonExistingModuleBounds() throws Exception {
        // Get the moduleBounds
        restModuleBoundsMockMvc.perform(get("/api/module-bounds/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModuleBounds() throws Exception {
        // Initialize the database
        moduleBoundsRepository.saveAndFlush(moduleBounds);
        int databaseSizeBeforeUpdate = moduleBoundsRepository.findAll().size();

        // Update the moduleBounds
        ModuleBounds updatedModuleBounds = new ModuleBounds();
        updatedModuleBounds.setId(moduleBounds.getId());
        updatedModuleBounds.setRowPos(UPDATED_ROW_POS);
        updatedModuleBounds.setColPos(UPDATED_COL_POS);
        updatedModuleBounds.setRowSpan(UPDATED_ROW_SPAN);
        updatedModuleBounds.setColSpan(UPDATED_COL_SPAN);
        ModuleBoundsDTO moduleBoundsDTO = moduleBoundsMapper.moduleBoundsToModuleBoundsDTO(updatedModuleBounds);

        restModuleBoundsMockMvc.perform(put("/api/module-bounds")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(moduleBoundsDTO)))
                .andExpect(status().isOk());

        // Validate the ModuleBounds in the database
        List<ModuleBounds> moduleBounds = moduleBoundsRepository.findAll();
        assertThat(moduleBounds).hasSize(databaseSizeBeforeUpdate);
        ModuleBounds testModuleBounds = moduleBounds.get(moduleBounds.size() - 1);
        assertThat(testModuleBounds.getRowPos()).isEqualTo(UPDATED_ROW_POS);
        assertThat(testModuleBounds.getColPos()).isEqualTo(UPDATED_COL_POS);
        assertThat(testModuleBounds.getRowSpan()).isEqualTo(UPDATED_ROW_SPAN);
        assertThat(testModuleBounds.getColSpan()).isEqualTo(UPDATED_COL_SPAN);
    }

    @Test
    @Transactional
    public void deleteModuleBounds() throws Exception {
        // Initialize the database
        moduleBoundsRepository.saveAndFlush(moduleBounds);
        int databaseSizeBeforeDelete = moduleBoundsRepository.findAll().size();

        // Get the moduleBounds
        restModuleBoundsMockMvc.perform(delete("/api/module-bounds/{id}", moduleBounds.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ModuleBounds> moduleBounds = moduleBoundsRepository.findAll();
        assertThat(moduleBounds).hasSize(databaseSizeBeforeDelete - 1);
    }
}
