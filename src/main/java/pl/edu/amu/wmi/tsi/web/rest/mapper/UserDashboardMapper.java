package pl.edu.amu.wmi.tsi.web.rest.mapper;

import pl.edu.amu.wmi.tsi.domain.*;
import pl.edu.amu.wmi.tsi.web.rest.dto.UserDashboardDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity UserDashboard and its DTO UserDashboardDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ModuleBoundsMapper.class})
public interface UserDashboardMapper {

    UserDashboardDTO userDashboardToUserDashboardDTO(UserDashboard userDashboard);

    List<UserDashboardDTO> userDashboardsToUserDashboardDTOs(List<UserDashboard> userDashboards);

    @Mapping(target = "user", ignore = true)
    //@Mapping(target = "moduleBoundss", ignore = true)
    UserDashboard userDashboardDTOToUserDashboard(UserDashboardDTO userDashboardDTO);

    List<UserDashboard> userDashboardDTOsToUserDashboards(List<UserDashboardDTO> userDashboardDTOs);
}
