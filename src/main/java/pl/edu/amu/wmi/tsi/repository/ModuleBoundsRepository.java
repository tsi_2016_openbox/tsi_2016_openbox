package pl.edu.amu.wmi.tsi.repository;

import pl.edu.amu.wmi.tsi.domain.Module;
import pl.edu.amu.wmi.tsi.domain.ModuleBounds;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ModuleBounds entity.
 */
public interface ModuleBoundsRepository extends JpaRepository<ModuleBounds,Long> {
}
