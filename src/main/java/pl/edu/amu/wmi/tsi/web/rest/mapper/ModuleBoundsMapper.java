package pl.edu.amu.wmi.tsi.web.rest.mapper;

import pl.edu.amu.wmi.tsi.domain.*;
import pl.edu.amu.wmi.tsi.web.rest.dto.ModuleBoundsDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity ModuleBounds and its DTO ModuleBoundsDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ModuleBoundsMapper {

    @Mapping(source = "module.id", target = "moduleId")
    @Mapping(source = "userDashboard.id", target = "userDashboardId")
    ModuleBoundsDTO moduleBoundsToModuleBoundsDTO(ModuleBounds moduleBounds);

    List<ModuleBoundsDTO> moduleBoundsToModuleBoundsDTOs(List<ModuleBounds> moduleBounds);

    @Mapping(source = "moduleId", target = "module")
    @Mapping(source = "userDashboardId", target = "userDashboard")
    ModuleBounds moduleBoundsDTOToModuleBounds(ModuleBoundsDTO moduleBoundsDTO);

    List<ModuleBounds> moduleBoundsDTOsToModuleBounds(List<ModuleBoundsDTO> moduleBoundsDTOs);

    default Module moduleFromId(Long id) {
        if (id == null) {
            return null;
        }
        Module module = new Module();
        module.setId(id);
        return module;
    }

    default UserDashboard userDashboardFromId(Long id) {
        if (id == null) {
            return null;
        }
        UserDashboard userDashboard = new UserDashboard();
        userDashboard.setId(id);
        return userDashboard;
    }
}
