package pl.edu.amu.wmi.tsi.repository;

import pl.edu.amu.wmi.tsi.domain.Module;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Module entity.
 */
public interface ModuleRepository extends JpaRepository<Module,Long> {

    @Query("select module from Module module where module.user_created_module.login = ?#{principal.username}")
    List<Module> findByUser_created_moduleIsCurrentUser();

}
