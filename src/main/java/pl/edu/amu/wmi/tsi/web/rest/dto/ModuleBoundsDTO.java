package pl.edu.amu.wmi.tsi.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the ModuleBounds entity.
 */
public class ModuleBoundsDTO implements Serializable {

    private Long id;

    private Integer rowPos;


    private Integer colPos;


    private Integer rowSpan;


    private Integer colSpan;


    private Long moduleId;
    private Long userDashboardId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Integer getRowPos() {
        return rowPos;
    }

    public void setRowPos(Integer rowPos) {
        this.rowPos = rowPos;
    }
    public Integer getColPos() {
        return colPos;
    }

    public void setColPos(Integer colPos) {
        this.colPos = colPos;
    }
    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }
    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }
    public Long getUserDashboardId() {
        return userDashboardId;
    }

    public void setUserDashboardId(Long userDashboardId) {
        this.userDashboardId = userDashboardId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModuleBoundsDTO moduleBoundsDTO = (ModuleBoundsDTO) o;

        if ( ! Objects.equals(id, moduleBoundsDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModuleBoundsDTO{" +
            "id=" + id +
            ", rowPos='" + rowPos + "'" +
            ", colPos='" + colPos + "'" +
            ", rowSpan='" + rowSpan + "'" +
            ", colSpan='" + colSpan + "'" +
            '}';
    }
}
