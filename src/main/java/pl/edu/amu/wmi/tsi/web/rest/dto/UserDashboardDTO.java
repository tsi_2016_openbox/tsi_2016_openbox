package pl.edu.amu.wmi.tsi.web.rest.dto;

import pl.edu.amu.wmi.tsi.domain.ModuleBounds;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the UserDashboard entity.
 */
public class UserDashboardDTO implements Serializable {

    private Long id;

    private String name;


    private Integer columns;

    private Set<ModuleBoundsDTO> moduleBoundss;


    public Set<ModuleBoundsDTO> getModuleBoundss() {
        return moduleBoundss;
    }

    public void setModuleBoundss(Set<ModuleBoundsDTO> moduleBoundss) {
        this.moduleBoundss = moduleBoundss;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDashboardDTO userDashboardDTO = (UserDashboardDTO) o;

        if ( ! Objects.equals(id, userDashboardDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UserDashboardDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", columns=" + columns +
            ", moduleBoundss=" + moduleBoundss +
            '}';
    }
}
