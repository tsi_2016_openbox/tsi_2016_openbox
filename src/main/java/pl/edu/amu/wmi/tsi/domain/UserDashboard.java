package pl.edu.amu.wmi.tsi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A UserDashboard.
 */
@Entity
@Table(name = "user_dashboard")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserDashboard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "columns")
    private Integer columns;

    @OneToOne(mappedBy = "userDashboard")
    @JsonIgnore
    private User user;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userDashboard", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<ModuleBounds> moduleBoundss = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getColumns() {
        return columns;
    }

    public void setColumns(Integer columns) {
        this.columns = columns;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<ModuleBounds> getModuleBoundss() {
        return moduleBoundss;
    }

    public void setModuleBoundss(Set<ModuleBounds> moduleBoundss) {
        this.moduleBoundss = moduleBoundss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDashboard userDashboard = (UserDashboard) o;
        if(userDashboard.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, userDashboard.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


    @Override
    public String toString() {
        return "UserDashboard{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", columns=" + columns +
            ", user=" + (user != null ? user.getId() : "null") +
            ", moduleBoundss=" + (moduleBoundss != null ? moduleBoundss.stream().map(ModuleBounds::getId).map(String::valueOf).collect(Collectors.joining(", ")) : "null") +
            '}';
    }
}
