package pl.edu.amu.wmi.tsi.repository;

import pl.edu.amu.wmi.tsi.domain.UserDashboard;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserDashboard entity.
 */
public interface UserDashboardRepository extends JpaRepository<UserDashboard,Long> {

}
