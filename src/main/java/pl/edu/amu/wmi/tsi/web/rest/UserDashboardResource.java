package pl.edu.amu.wmi.tsi.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.edu.amu.wmi.tsi.domain.UserDashboard;
import pl.edu.amu.wmi.tsi.repository.UserDashboardRepository;
import pl.edu.amu.wmi.tsi.web.rest.util.HeaderUtil;
import pl.edu.amu.wmi.tsi.web.rest.util.PaginationUtil;
import pl.edu.amu.wmi.tsi.web.rest.dto.UserDashboardDTO;
import pl.edu.amu.wmi.tsi.web.rest.mapper.UserDashboardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing UserDashboard.
 */
@RestController
@RequestMapping("/api")
public class UserDashboardResource {

    private final Logger log = LoggerFactory.getLogger(UserDashboardResource.class);
        
    @Inject
    private UserDashboardRepository userDashboardRepository;
    
    @Inject
    private UserDashboardMapper userDashboardMapper;
    
    /**
     * POST  /user-dashboards : Create a new userDashboard.
     *
     * @param userDashboardDTO the userDashboardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userDashboardDTO, or with status 400 (Bad Request) if the userDashboard has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-dashboards",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserDashboardDTO> createUserDashboard(@RequestBody UserDashboardDTO userDashboardDTO) throws URISyntaxException {
        log.debug("REST request to save UserDashboard : {}", userDashboardDTO);
        if (userDashboardDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userDashboard", "idexists", "A new userDashboard cannot already have an ID")).body(null);
        }
        UserDashboard userDashboard = userDashboardMapper.userDashboardDTOToUserDashboard(userDashboardDTO);
        userDashboard = userDashboardRepository.save(userDashboard);
        UserDashboardDTO result = userDashboardMapper.userDashboardToUserDashboardDTO(userDashboard);
        return ResponseEntity.created(new URI("/api/user-dashboards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userDashboard", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-dashboards : Updates an existing userDashboard.
     *
     * @param userDashboardDTO the userDashboardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userDashboardDTO,
     * or with status 400 (Bad Request) if the userDashboardDTO is not valid,
     * or with status 500 (Internal Server Error) if the userDashboardDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-dashboards",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserDashboardDTO> updateUserDashboard(@RequestBody UserDashboardDTO userDashboardDTO) throws URISyntaxException {
        log.debug("REST request to update UserDashboard : {}", userDashboardDTO);
        if (userDashboardDTO.getId() == null) {
            return createUserDashboard(userDashboardDTO);
        }
        UserDashboard userDashboard = userDashboardMapper.userDashboardDTOToUserDashboard(userDashboardDTO);
        userDashboard = userDashboardRepository.save(userDashboard);
        UserDashboardDTO result = userDashboardMapper.userDashboardToUserDashboardDTO(userDashboard);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userDashboard", userDashboardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-dashboards : get all the userDashboards.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of userDashboards in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/user-dashboards",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<UserDashboardDTO>> getAllUserDashboards(Pageable pageable, @RequestParam(required = false) String filter)
        throws URISyntaxException {
        if ("user-is-null".equals(filter)) {
            log.debug("REST request to get all UserDashboards where user is null");
            return new ResponseEntity<>(StreamSupport
                .stream(userDashboardRepository.findAll().spliterator(), false)
                .filter(userDashboard -> userDashboard.getUser() == null)
                .map(userDashboardMapper::userDashboardToUserDashboardDTO)
                .collect(Collectors.toList()), HttpStatus.OK);
        }
        log.debug("REST request to get a page of UserDashboards");
        Page<UserDashboard> page = userDashboardRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-dashboards");
        return new ResponseEntity<>(userDashboardMapper.userDashboardsToUserDashboardDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-dashboards/:id : get the "id" userDashboard.
     *
     * @param id the id of the userDashboardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userDashboardDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/user-dashboards/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserDashboardDTO> getUserDashboard(@PathVariable Long id) {
        log.debug("REST request to get UserDashboard : {}", id);
        UserDashboard userDashboard = userDashboardRepository.findOne(id);
        UserDashboardDTO userDashboardDTO = userDashboardMapper.userDashboardToUserDashboardDTO(userDashboard);
        return Optional.ofNullable(userDashboardDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /user-dashboards/:id : delete the "id" userDashboard.
     *
     * @param id the id of the userDashboardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/user-dashboards/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserDashboard(@PathVariable Long id) {
        log.debug("REST request to delete UserDashboard : {}", id);
        userDashboardRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userDashboard", id.toString())).build();
    }

}
