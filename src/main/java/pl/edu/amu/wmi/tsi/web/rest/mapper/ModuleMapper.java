package pl.edu.amu.wmi.tsi.web.rest.mapper;

import pl.edu.amu.wmi.tsi.domain.*;
import pl.edu.amu.wmi.tsi.web.rest.dto.ModuleDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Module and its DTO ModuleDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface ModuleMapper {

    @Mapping(source = "user_created_module.id", target = "user_created_moduleId")
    @Mapping(source = "user_created_module.login", target = "user_created_moduleLogin")
    ModuleDTO moduleToModuleDTO(Module module);

    List<ModuleDTO> modulesToModuleDTOs(List<Module> modules);

    @Mapping(source = "user_created_moduleId", target = "user_created_module")
    Module moduleDTOToModule(ModuleDTO moduleDTO);

    List<Module> moduleDTOsToModules(List<ModuleDTO> moduleDTOs);
}
