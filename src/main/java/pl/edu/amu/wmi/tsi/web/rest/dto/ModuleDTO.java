package pl.edu.amu.wmi.tsi.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the Module entity.
 */
public class ModuleDTO implements Serializable {

    private Long id;

    private String name;


    private Boolean enabled;


    @Lob
    private String code;


    private Long user_created_moduleId;

    private String user_created_moduleLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getUser_created_moduleId() {
        return user_created_moduleId;
    }

    public void setUser_created_moduleId(Long userId) {
        this.user_created_moduleId = userId;
    }

    public String getUser_created_moduleLogin() {
        return user_created_moduleLogin;
    }

    public void setUser_created_moduleLogin(String userLogin) {
        this.user_created_moduleLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModuleDTO moduleDTO = (ModuleDTO) o;

        if ( ! Objects.equals(id, moduleDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModuleDTO{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", enabled='" + enabled + "'" +
            ", code='" + code + "'" +
            '}';
    }
}
