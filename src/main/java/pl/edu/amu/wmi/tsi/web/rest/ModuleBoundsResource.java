package pl.edu.amu.wmi.tsi.web.rest;

import com.codahale.metrics.annotation.Timed;
import pl.edu.amu.wmi.tsi.domain.ModuleBounds;
import pl.edu.amu.wmi.tsi.domain.UserDashboard;
import pl.edu.amu.wmi.tsi.repository.ModuleBoundsRepository;
import pl.edu.amu.wmi.tsi.repository.ModuleRepository;
import pl.edu.amu.wmi.tsi.repository.UserDashboardRepository;
import pl.edu.amu.wmi.tsi.web.rest.util.HeaderUtil;
import pl.edu.amu.wmi.tsi.web.rest.util.PaginationUtil;
import pl.edu.amu.wmi.tsi.web.rest.dto.ModuleBoundsDTO;
import pl.edu.amu.wmi.tsi.web.rest.mapper.ModuleBoundsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing ModuleBounds.
 */
@RestController
@RequestMapping("/api")
public class ModuleBoundsResource {

    private final Logger log = LoggerFactory.getLogger(ModuleBoundsResource.class);

    @Inject
    private ModuleBoundsRepository moduleBoundsRepository;

    @Inject
    private UserDashboardRepository userDashboardRepository;

    @Inject
    private ModuleBoundsMapper moduleBoundsMapper;

    /**
     * POST  /module-bounds : Create a new moduleBounds.
     *
     * @param moduleBoundsDTO the moduleBoundsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new moduleBoundsDTO, or with status 400 (Bad Request) if the moduleBounds has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/module-bounds",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModuleBoundsDTO> createModuleBounds(@RequestBody ModuleBoundsDTO moduleBoundsDTO) throws URISyntaxException {
        log.debug("REST request to save ModuleBounds : {}", moduleBoundsDTO);
        if (moduleBoundsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("moduleBounds", "idexists", "A new moduleBounds cannot already have an ID")).body(null);
        }
        ModuleBounds moduleBounds = moduleBoundsMapper.moduleBoundsDTOToModuleBounds(moduleBoundsDTO);
        moduleBounds = moduleBoundsRepository.save(moduleBounds);
        ModuleBoundsDTO result = moduleBoundsMapper.moduleBoundsToModuleBoundsDTO(moduleBounds);
        return ResponseEntity.created(new URI("/api/module-bounds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("moduleBounds", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /module-bounds : Updates an existing moduleBounds.
     *
     * @param moduleBoundsDTO the moduleBoundsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated moduleBoundsDTO,
     * or with status 400 (Bad Request) if the moduleBoundsDTO is not valid,
     * or with status 500 (Internal Server Error) if the moduleBoundsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/module-bounds",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModuleBoundsDTO> updateModuleBounds(@RequestBody ModuleBoundsDTO moduleBoundsDTO) throws URISyntaxException {
        log.debug("REST request to update ModuleBounds : {}", moduleBoundsDTO);
        if (moduleBoundsDTO.getId() == null) {
            return createModuleBounds(moduleBoundsDTO);
        }
        ModuleBounds moduleBounds = moduleBoundsMapper.moduleBoundsDTOToModuleBounds(moduleBoundsDTO);
        moduleBounds = moduleBoundsRepository.save(moduleBounds);
        ModuleBoundsDTO result = moduleBoundsMapper.moduleBoundsToModuleBoundsDTO(moduleBounds);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("moduleBounds", moduleBoundsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /module-bounds : get all the moduleBounds.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of moduleBounds in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/module-bounds",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ModuleBoundsDTO>> getAllModuleBounds(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of ModuleBounds");
        Page<ModuleBounds> page = moduleBoundsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/module-bounds");
        return new ResponseEntity<>(moduleBoundsMapper.moduleBoundsToModuleBoundsDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /module-bounds/:id : get the "id" moduleBounds.
     *
     * @param id the id of the moduleBoundsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the moduleBoundsDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/module-bounds/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ModuleBoundsDTO> getModuleBounds(@PathVariable Long id) {
        log.debug("REST request to get ModuleBounds : {}", id);
        ModuleBounds moduleBounds = moduleBoundsRepository.findOne(id);
        ModuleBoundsDTO moduleBoundsDTO = moduleBoundsMapper.moduleBoundsToModuleBoundsDTO(moduleBounds);
        return Optional.ofNullable(moduleBoundsDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /module-bounds/:id : delete the "id" moduleBounds.
     *
     * @param id the id of the moduleBoundsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/module-bounds/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteModuleBounds(@PathVariable Long id) {
        log.debug("REST request to delete ModuleBounds : {}", id);
        ModuleBounds moduleBounds = moduleBoundsRepository.findOne(id);
        UserDashboard userDashboard = moduleBounds.getUserDashboard();
        userDashboard.getModuleBoundss().remove(moduleBounds);
        userDashboardRepository.save(userDashboard);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("moduleBounds", id.toString())).build();
    }

}
