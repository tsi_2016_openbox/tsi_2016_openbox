package pl.edu.amu.wmi.tsi.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ModuleBounds.
 */
@Entity
@Table(name = "module_bounds")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ModuleBounds implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "row_pos")
    private Integer rowPos;

    @Column(name = "col_pos")
    private Integer colPos;

    @Column(name = "row_span")
    private Integer rowSpan;

    @Column(name = "col_span")
    private Integer colSpan;

    @ManyToOne
    private Module module;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private UserDashboard userDashboard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRowPos() {
        return rowPos;
    }

    public void setRowPos(Integer rowPos) {
        this.rowPos = rowPos;
    }

    public Integer getColPos() {
        return colPos;
    }

    public void setColPos(Integer colPos) {
        this.colPos = colPos;
    }

    public Integer getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Integer rowSpan) {
        this.rowSpan = rowSpan;
    }

    public Integer getColSpan() {
        return colSpan;
    }

    public void setColSpan(Integer colSpan) {
        this.colSpan = colSpan;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public UserDashboard getUserDashboard() {
        return userDashboard;
    }

    public void setUserDashboard(UserDashboard userDashboard) {
        this.userDashboard = userDashboard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModuleBounds moduleBounds = (ModuleBounds) o;
        if(moduleBounds.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, moduleBounds.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ModuleBounds{" +
            "id=" + id +
            ", rowPos=" + rowPos +
            ", colPos=" + colPos +
            ", rowSpan=" + rowSpan +
            ", colSpan=" + colSpan +
            ", module=" + module +
            ", userDashboard=" + userDashboard +
            '}';
    }
}
