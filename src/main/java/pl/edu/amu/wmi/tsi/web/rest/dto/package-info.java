/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package pl.edu.amu.wmi.tsi.web.rest.dto;
