'use strict';

angular.module('openBoxApp')
    .config(function ($httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })
    .factory('BootSwatchService', function ($http) {
        return {
            get: function() {
                return $http({
                    method: "GET",
                    url: "http://bootswatch.com/api/3.json",
                    withCredentials: false
                }).then(function (response) {
                        return response.data.themes;
                });
            }
        };
    });
