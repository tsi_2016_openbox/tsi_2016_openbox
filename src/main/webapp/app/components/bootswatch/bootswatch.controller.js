'use strict';

angular.module('openBoxApp')
    .controller('BootswatchController', function ($scope, BootSwatchService) {
        /*Get the list of availabel bootswatch themes*/
        BootSwatchService.get().then(function(themes) {
            $scope.themes = themes;
            $scope.themes.unshift({name:'Default',css:''});
            var defaultTitle = $("#bootswatch-css").attr("title");
            Array.prototype.forEach.call(themes, function(elem){
                if(elem.name === defaultTitle) {
                    $("#bootswatch-css").attr("href", elem.css);
                }
            });
        });
    });
