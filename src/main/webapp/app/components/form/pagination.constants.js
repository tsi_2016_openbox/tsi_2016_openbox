(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
