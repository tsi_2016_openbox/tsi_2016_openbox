'use strict';

angular.module('openBoxApp')
    .factory('BootswatchInterceptor', function ($rootScope, $q, $location, $localStorage) {
        return {
            // Add authorization token to headers
            request: function (config) {
                config.headers = config.headers || {};
                // exclude bootswatch url
                if(config.url.indexOf('api.bootswatch.com') !== -1){

                  delete config.headers['Authorization'];

                }
                return config;
            }
        };
    });
