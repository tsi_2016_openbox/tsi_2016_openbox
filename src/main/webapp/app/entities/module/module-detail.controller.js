(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleDetailController', ModuleDetailController);

    ModuleDetailController.$inject = ['$scope', '$rootScope', '$sce', '$stateParams', 'DataUtils', 'entity', 'Module', 'User'];

    function ModuleDetailController($scope, $rootScope, $sce, $stateParams, DataUtils, entity, Module, User) {
        var vm = this;
        vm.module = entity;
        vm.load = function (id) {
            Module.get({id: id}, function(result) {
                vm.module = result;
            });
        };
        var unsubscribe = $rootScope.$on('openBoxApp:moduleUpdate', function(event, result) {
            vm.module = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        $scope.deliberatelyTrustDangerousSnippet = function() {
            return $sce.trustAsHtml($scope.vm.module.code);
        };
    }
})();
