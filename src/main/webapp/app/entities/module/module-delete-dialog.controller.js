(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleDeleteController',ModuleDeleteController);

    ModuleDeleteController.$inject = ['$uibModalInstance', 'entity', 'Module'];

    function ModuleDeleteController($uibModalInstance, entity, Module) {
        var vm = this;
        vm.module = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Module.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
