(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleController', ModuleController);

    ModuleController.$inject = ['$scope', '$state', 'DataUtils', 'Module', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Principal'];

    function ModuleController ($scope, $state, DataUtils, Module, ParseLinks, AlertService, pagingParams, paginationConstants, Principal) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.loadAll();
        Principal.hasAuthority('ROLE_ADMIN').then(function(res){
            vm.isAdmin = res;
        });

        function loadAll () {
            Module.query({
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                if(vm.isAdmin){
                    vm.modules = data;
                } else {
                    vm.modules = data.filter(function(module){
                        return module.enabled;
                    });
                }
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

    }
})();
