(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleDialogController', ModuleDialogController);

    ModuleDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Module', 'User'];

    function ModuleDialogController ($scope, $stateParams, $uibModalInstance, DataUtils, entity, Module, User) {
        var vm = this;
        vm.module = entity;
        vm.users = User.query();
        vm.load = function(id) {
            Module.get({id : id}, function(result) {
                vm.module = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('openBoxApp:moduleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.module.id !== null) {
                Module.update(vm.module, onSaveSuccess, onSaveError);
            } else {
                Module.save(vm.module, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
    }
})();
