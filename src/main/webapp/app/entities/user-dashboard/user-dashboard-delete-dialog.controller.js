(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('UserDashboardDeleteController',UserDashboardDeleteController);

    UserDashboardDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserDashboard'];

    function UserDashboardDeleteController($uibModalInstance, entity, UserDashboard) {
        var vm = this;
        vm.userDashboard = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            UserDashboard.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
