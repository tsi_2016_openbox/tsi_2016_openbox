(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('UserDashboardAddDialogController', UserDashboardAddDialogController);

    UserDashboardAddDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserDashboard', 'User', 'Module', 'ModuleBounds'];

    function UserDashboardAddDialogController ($scope, $stateParams, $uibModalInstance, entity, UserDashboard, User, Module, ModuleBounds) {
        var vm = this;
        Module.query({size: 100}, function(result){
            vm.modules = result.filter(function(module){
                return module.enabled;
            });
        });
        vm.userDashboard = entity;
        vm.users = User.query();
        vm.moduleboundss = ModuleBounds.query();
        vm.load = function(id) {
            UserDashboard.get({id : id}, function(result) {
                vm.userDashboard = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('openBoxApp:userDashboardUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            if(!vm.pickedModuleId){
                return;
            }
            vm.isSaving = true;
            console.log();
            var maxRow = 0;
            vm.userDashboard.moduleBoundss.forEach(function(elem){
                if(elem.rowPos >= maxRow){
                    maxRow = elem.rowPos + 1;
                }
            });
            vm.userDashboard.moduleBoundss.push({
                rowPos: maxRow,
                colPos: 0,
                colSpan: 1,
                rowSpan: 1,
                userDashboardId: vm.userDashboard.id,
                moduleId: vm.pickedModuleId
            });
            if (vm.userDashboard.id !== null) {
                UserDashboard.update(vm.userDashboard, onSaveSuccess, onSaveError);
            } else {
                UserDashboard.save(vm.userDashboard, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
