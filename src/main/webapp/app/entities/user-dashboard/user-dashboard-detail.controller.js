(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('UserDashboardDetailController', UserDashboardDetailController);

    UserDashboardDetailController.$inject = ['$scope', '$rootScope', '$sce', '$state', '$stateParams', 'entity', 'UserDashboard', 'Module', 'User', 'ModuleBounds', 'Principal'];

    function UserDashboardDetailController($scope, $rootScope, $sce, $state, $stateParams, entity, UserDashboard, Module, User, ModuleBounds, Principal) {
        var vm = this;
        vm.userDashboard = entity;

        $scope.admin = Principal.hasAuthority('ROLE_ADMIN');

        $scope.editMode = !$stateParams.noEditAtStart;

        $scope.config = {
            autoHideScrollbar: false,
            theme: 'minimal-dark',
            advanced:{
                updateOnContentResize: true
            },
            setHeight: 200,
            scrollInertia: 0
        };


        $scope.gridsterOpts = {
            columns: 6, // the width of the grid, in columns
            pushing: true, // whether to push other items out of the way on move or resize
            floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
            swapping: false, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
            width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
            colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
            rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
            margins: [10, 10], // the pixel distance between each widget
            outerMargin: true, // whether margins apply to outer edges of the grid
            isMobile: false, // stacks the grid items if true
            mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
            mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
            minColumns: 1, // the minimum columns the grid must have
            minRows: 1, // the minimum height of the grid, in rows
            maxRows: 100,
            defaultSizeX: 2, // the default width of a gridster item, if not specifed
            defaultSizeY: 1, // the default height of a gridster item, if not specified
            minSizeX: 1, // minimum column width of an item
            maxSizeX: 100, // maximum column width of an item
            minSizeY: 1, // minumum row height of an item
            maxSizeY: 100, // maximum row height of an item
            draggable: {
                enabled: $scope.editMode
            },
            resizable: {
                enabled: $scope.editMode,
                handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw']
            }
        };

        $scope.toggleEditMode = function(){
            $scope.editMode = !$scope.editMode;
            $scope.gridsterOpts.draggable.enabled = $scope.editMode;
            $scope.gridsterOpts.resizable.enabled = $scope.editMode;
        };

        $scope.items = [];

        entity.$promise.then(function(val){
            $scope.gridsterOpts.columns = val.columns;

            val.moduleBoundss.forEach(function(item){
                Module.get({id: item.moduleId}, function(result) {
                    $scope.items.push({
                       item: item,
                       sizeX: item.colSpan,
                       sizeY: item.rowSpan,
                       row: item.rowPos,
                       col: item.colPos,
                       module: result
                    });
                });
            });

            setTimeout(function() {
                var resetOnWatch = function(oldVal, newVal){
                    resetState($('#saveButton'));
                };

                $scope.$watch('gridsterOpts.columns', resetOnWatch, true);
                $scope.$watch('items', resetOnWatch, true);
            }, 2000);
        });
        vm.load = function (id) {
            UserDashboard.get({id: id}, function(result) {
                vm.userDashboard = result;
            });
        };
        var unsubscribe = $rootScope.$on('openBoxApp:userDashboardUpdate', function(event, result) {
            vm.userDashboard = result;
        });
        $scope.$on('$destroy', unsubscribe);

        var resetState = function(elem){
            $(elem).removeClass('btn-success');
            $(elem).addClass('btn-danger');
            $(elem).prop('disabled', false);
        };

        var saveState = function(elem){
            $(elem).addClass('btn-success');
            $(elem).removeClass('btn-danger');
            $(elem).prop('disabled', true);
        };

        $scope.save = function(evt){
            var elem = evt.target;
            vm.userDashboard.columns = $scope.gridsterOpts.columns;
            vm.userDashboard.moduleBoundss = [];
            $scope.items.forEach(function(elem){
                elem.item.rowSpan = elem.sizeY;
                elem.item.colSpan = elem.sizeX;
                elem.item.rowPos = elem.row;
                elem.item.colPos = elem.col;
                vm.userDashboard.moduleBoundss.push(elem.item);
            });
            console.log(vm.userDashboard);

            UserDashboard.update(vm.userDashboard, function(response){
                saveState(elem);
            }, function(err){
                resetState(elem);
            });
        };

        $scope.deliberatelyTrustDangerousSnippet = function(code) {
            return $sce.trustAsHtml(code);
        };

        $scope.closeModule = function(item){
            //console.log(item.item.id);
            var indexOfItem = $scope.items.indexOf(item);
            if (indexOfItem > -1) {
                $scope.items.splice(indexOfItem, 1);
            }
            //ModuleBounds.delete({id: item.item.id}, function() {
            //    $state.go($state.current, $stateParams, {reload: true});
            //}, function(err) {
            //    console.log(err);
            //});
        };
    }
})();
