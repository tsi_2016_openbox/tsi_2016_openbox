(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('UserDashboardDialogController', UserDashboardDialogController);

    UserDashboardDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserDashboard', 'User', 'ModuleBounds'];

    function UserDashboardDialogController ($scope, $stateParams, $uibModalInstance, entity, UserDashboard, User, ModuleBounds) {
        var vm = this;
        vm.userDashboard = entity;
        vm.users = User.query();
        vm.moduleboundss = ModuleBounds.query();
        vm.load = function(id) {
            UserDashboard.get({id : id}, function(result) {
                vm.userDashboard = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('openBoxApp:userDashboardUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.userDashboard.id !== null) {
                UserDashboard.update(vm.userDashboard, onSaveSuccess, onSaveError);
            } else {
                UserDashboard.save(vm.userDashboard, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
