(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-dashboard', {
            parent: 'entity',
            url: '/user-dashboard',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'openBoxApp.userDashboard.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-dashboard/user-dashboards.html',
                    controller: 'UserDashboardController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userDashboard');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-dashboard-detail', {
            parent: 'entity',
            url: '/user-dashboard/{id:[0-9]+}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'openBoxApp.userDashboard.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-detail.html',
                    controller: 'UserDashboardDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userDashboard');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserDashboard', function($stateParams, UserDashboard) {
                    return UserDashboard.get({id : $stateParams.id});
                }]
            }
        })
        .state('just-user-dashboard-detail', {
            parent: 'entity',
            url: '/my-user-dashboard',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'openBoxApp.userDashboard.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-detail.html',
                    controller: 'UserDashboardDetailController',
                    controllerAs: 'vm'
                }
            },
            params: { noEditAtStart: true },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userDashboard');
                    return $translate.refresh();
                }],
                entity: ['UserDashboard', 'Principal', function(UserDashboard, Principal) {
                    return Principal.identity().then(function(_identity){
                        return UserDashboard.get({id : _identity.userDashboardId});
                    });
                }]
            }
        })
        .state('just-user-dashboard-detail.add', {
            parent: 'just-user-dashboard-detail',
            url: '/my-user-dashboard/add',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$state', '$uibModal', function($state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-add-dialog.html',
                    controller: 'UserDashboardAddDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Principal', 'UserDashboard', function(Principal, UserDashboard) {
                            return Principal.identity().then(function(_identity){
                                return UserDashboard.get({id : _identity.userDashboardId});
                            });
                        }]
                    }
                }).result.then(function() {
                    $state.go('just-user-dashboard-detail', { noEditAtStart: false }, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('just-user-dashboard-detail.edit', {
            parent: 'just-user-dashboard-detail',
            url: '/my-user-dashboard/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$state', '$uibModal', function($state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-dialog.html',
                    controller: 'UserDashboardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Principal', 'UserDashboard', function(Principal, UserDashboard) {
                            return Principal.identity().then(function(_identity){
                                return UserDashboard.get({id : _identity.userDashboardId});
                            });
                        }]
                    }
                }).result.then(function() {
                    $state.go('just-user-dashboard-detail', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-dashboard-detail.add', {
            parent: 'user-dashboard-detail',
            url: '/user-dashboard/{id:[0-9]+}/add',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-add-dialog.html',
                    controller: 'UserDashboardAddDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserDashboard', function(UserDashboard) {
                            return UserDashboard.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-dashboard-detail', {id : $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-dashboard-detail.edit', {
            parent: 'user-dashboard-detail',
            url: '/user-dashboard/{id:[0-9]+}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-dialog.html',
                    controller: 'UserDashboardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserDashboard', function(UserDashboard) {
                            return UserDashboard.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-dashboard-detail', {id : $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-dashboard.new', {
            parent: 'user-dashboard',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-dialog.html',
                    controller: 'UserDashboardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                columns: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-dashboard', null, { reload: true });
                }, function() {
                    $state.go('user-dashboard');
                });
            }]
        })
        .state('user-dashboard.edit', {
            parent: 'user-dashboard',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-dialog.html',
                    controller: 'UserDashboardDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserDashboard', function(UserDashboard) {
                            return UserDashboard.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-dashboard', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-dashboard.delete', {
            parent: 'user-dashboard',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-dashboard/user-dashboard-delete-dialog.html',
                    controller: 'UserDashboardDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserDashboard', function(UserDashboard) {
                            return UserDashboard.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-dashboard', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
