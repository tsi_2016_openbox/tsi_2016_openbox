(function() {
    'use strict';
    angular
        .module('openBoxApp')
        .factory('ModuleBounds', ModuleBounds);

    ModuleBounds.$inject = ['$resource'];

    function ModuleBounds ($resource) {
        var resourceUrl =  'api/module-bounds/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' },
            'delete': { method:'DELETE' }
        });
    }
})();
