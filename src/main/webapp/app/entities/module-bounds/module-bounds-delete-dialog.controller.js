(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleBoundsDeleteController',ModuleBoundsDeleteController);

    ModuleBoundsDeleteController.$inject = ['$uibModalInstance', 'entity', 'ModuleBounds'];

    function ModuleBoundsDeleteController($uibModalInstance, entity, ModuleBounds) {
        var vm = this;
        vm.moduleBounds = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            ModuleBounds.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
