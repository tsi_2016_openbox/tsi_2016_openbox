(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('module-bounds', {
            parent: 'entity',
            url: '/module-bounds?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'openBoxApp.moduleBounds.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/module-bounds/module-bounds.html',
                    controller: 'ModuleBoundsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('moduleBounds');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('module-bounds-detail', {
            parent: 'entity',
            url: '/module-bounds/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'openBoxApp.moduleBounds.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/module-bounds/module-bounds-detail.html',
                    controller: 'ModuleBoundsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('moduleBounds');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ModuleBounds', function($stateParams, ModuleBounds) {
                    return ModuleBounds.get({id : $stateParams.id});
                }]
            }
        })
        .state('module-bounds.new', {
            parent: 'module-bounds',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/module-bounds/module-bounds-dialog.html',
                    controller: 'ModuleBoundsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                rowPos: null,
                                colPos: null,
                                rowSpan: null,
                                colSpan: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('module-bounds', null, { reload: true });
                }, function() {
                    $state.go('module-bounds');
                });
            }]
        })
        .state('module-bounds.edit', {
            parent: 'module-bounds',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/module-bounds/module-bounds-dialog.html',
                    controller: 'ModuleBoundsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ModuleBounds', function(ModuleBounds) {
                            return ModuleBounds.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('module-bounds', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('module-bounds.delete', {
            parent: 'module-bounds',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/module-bounds/module-bounds-delete-dialog.html',
                    controller: 'ModuleBoundsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ModuleBounds', function(ModuleBounds) {
                            return ModuleBounds.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('module-bounds', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
