(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleBoundsDetailController', ModuleBoundsDetailController);

    ModuleBoundsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'ModuleBounds', 'Module', 'UserDashboard'];

    function ModuleBoundsDetailController($scope, $rootScope, $stateParams, entity, ModuleBounds, Module, UserDashboard) {
        var vm = this;
        vm.moduleBounds = entity;
        vm.load = function (id) {
            ModuleBounds.get({id: id}, function(result) {
                vm.moduleBounds = result;
            });
        };
        var unsubscribe = $rootScope.$on('openBoxApp:moduleBoundsUpdate', function(event, result) {
            vm.moduleBounds = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
