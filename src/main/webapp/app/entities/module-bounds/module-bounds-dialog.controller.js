(function() {
    'use strict';

    angular
        .module('openBoxApp')
        .controller('ModuleBoundsDialogController', ModuleBoundsDialogController);

    ModuleBoundsDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ModuleBounds', 'Module', 'UserDashboard'];

    function ModuleBoundsDialogController ($scope, $stateParams, $uibModalInstance, entity, ModuleBounds, Module, UserDashboard) {
        var vm = this;
        vm.moduleBounds = entity;
        vm.modules = Module.query();
        vm.userdashboards = UserDashboard.query();
        vm.load = function(id) {
            ModuleBounds.get({id : id}, function(result) {
                vm.moduleBounds = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('openBoxApp:moduleBoundsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.moduleBounds.id !== null) {
                ModuleBounds.update(vm.moduleBounds, onSaveSuccess, onSaveError);
            } else {
                ModuleBounds.save(vm.moduleBounds, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
